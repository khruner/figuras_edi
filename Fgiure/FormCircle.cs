﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Fgiure.Model;

namespace Fgiure
{
    public partial class FormCircle : Form
    {
        public FormCircle(Form form)
        {
            InitializeComponent();
            this.Owner = form;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            FormIndex frm = new FormIndex();
            frm.Show();
        }

        private Circle createCircle(double radius) 
        {
            Circle circle = new Circle(radius);
            return circle;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Circle circle = createCircle(Convert.ToDouble(tbRadius.Text));
            ((FormIndex)Owner).AddFigure(circle);
            this.Hide();
            Owner.Show();
        }
    }
}
