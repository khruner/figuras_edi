﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fgiure.Controller
{
    internal class FigureController//implementar singleton
    {
        private static FigureController instance;
        private FigureController() { }
        public static FigureController GetInstance() 
        {
            if (instance == null) 
            {
                instance = new FigureController();
            }
            return instance; 
        }
        public void CreateSquare(double side) { }
    }
}
