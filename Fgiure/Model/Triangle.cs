﻿using Fgiure.Interaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fgiure.Model
{
    internal class Triangle : Ifigure
    {
        private int Id;
        private double sideA;
        private double sideB;
        private double under;
        private double height;
        private double semiperimeter;
        public string name;



        public Triangle(double sideA, double sideB, double under)
        {
            this.sideA = sideA;
            this.sideB = sideB;
            this.under = under;
            this.name = this.getName();
        }


        public string getName()
        {
            name = "Triangle";
            return name;
        }
        public string getDatos()
        {
            return "El triangulo Nro " + Id +
                "tiene una superficie de " + getSurface() +
                "con un perimetro de " + getPerimeter();
        }

        public double getPerimeter()
        {
            return sideA + sideB + under;
        }


        public double getSurface()
        {
            if (sideA == sideB && sideB == under) 
            {
                height = (sideA * Math.Sqrt(3)) / 2;
            }
            else if(sideA==sideB || sideA==under || sideB==under)
            {
                height= Math.Sqrt(Math.Pow(sideA, 2) - Math.Pow((under / 2),2));
            }
            else 
            {
                semiperimeter=(sideA+sideB+under)/2;
                height = (2 / under) * Math.Sqrt(semiperimeter * (semiperimeter - sideA) * (semiperimeter - sideB) * (semiperimeter - under));
                
            }
            return (under * height) / 2;
        }

    }
}
