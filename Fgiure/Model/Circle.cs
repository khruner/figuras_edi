﻿using Fgiure.Interaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fgiure.Model
{
    internal class Circle : Ifigure
    {
        private int id;
        private double radius;
        public string name;

        public Circle(double radius)
        {
            this.radius = radius;
            this.name = this.getName();
        }
        public string getName() 
        {
            name = "Circle";
            return name;
        }
        public string getDatos()
        {
            return "El cuadrado Nro " + id +
                "tiene una superficie de " + getSurface() +
                "con un perimetro de " + getPerimeter();
        }

        public double getPerimeter()
        {
            return 2 * Math.PI * radius;
        }

        public double getSurface()
        {
            return Math.PI * Math.Pow(radius, 2);
        }
    }

}
