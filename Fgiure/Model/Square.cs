﻿using Fgiure.Interaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Fgiure.Model
{
    internal class Square : Ifigure
    {
        private int id;
        private double side;
        public string name;


        public Square(double side)
        {
            this.side = side;
           
        }

        public string getName()
        {
            name = "Square";
            return name;
        }
        public string getDatos()
        {
            return "El cuadrado Nro " + id +
                "tiene una superficie de " + getSurface()+
                "con un perimetro de "+ getPerimeter();
        }

        public double getPerimeter()
        {
            return side * 4;
        }

        public double getSurface()
        {
            return side * side;
        }

    }
}
