﻿using Fgiure.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fgiure
{
    public partial class FormTriangle : Form
    {
        public FormTriangle(Form form)
        {
            InitializeComponent();
            this.Owner = form;
        }

        private Triangle CreateTriangle(double sideA, double sideB, double bottomSide)
        {
            Triangle triangle = new Triangle(sideA, sideB, bottomSide);
            return triangle;
        }


        private void btnCreateTriangle_Click(object sender, EventArgs e)
        {
            Triangle triangle = CreateTriangle(Convert.ToDouble(tbSideA.Text),
                Convert.ToDouble(tbSideB.Text),
                Convert.ToDouble(tbBottomSide.Text));
            ((FormIndex)Owner).AddFigure(triangle);
            this.Hide();
            Owner.Show();
        }

        private void btnCancelTriang(object sender, EventArgs e)
        {
            this.Hide();
            FormIndex frm = new FormIndex();
            frm.Show();
        }
    }
}
