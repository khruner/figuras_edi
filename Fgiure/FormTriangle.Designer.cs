﻿namespace Fgiure
{
    partial class FormTriangle
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tbSideA = new System.Windows.Forms.TextBox();
            this.tbSideB = new System.Windows.Forms.TextBox();
            this.tbBottomSide = new System.Windows.Forms.TextBox();
            this.btnCreateTriangle = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(184, 18);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Triangle";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 63);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Side A";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 104);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Side B";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 151);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Bottom side";
            // 
            // tbSideA
            // 
            this.tbSideA.Location = new System.Drawing.Point(73, 57);
            this.tbSideA.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbSideA.Name = "tbSideA";
            this.tbSideA.Size = new System.Drawing.Size(76, 20);
            this.tbSideA.TabIndex = 4;
            // 
            // tbSideB
            // 
            this.tbSideB.Location = new System.Drawing.Point(73, 104);
            this.tbSideB.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbSideB.Name = "tbSideB";
            this.tbSideB.Size = new System.Drawing.Size(76, 20);
            this.tbSideB.TabIndex = 5;
            // 
            // tbBottomSide
            // 
            this.tbBottomSide.Location = new System.Drawing.Point(73, 148);
            this.tbBottomSide.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbBottomSide.Name = "tbBottomSide";
            this.tbBottomSide.Size = new System.Drawing.Size(76, 20);
            this.tbBottomSide.TabIndex = 6;
            // 
            // btnCreateTriangle
            // 
            this.btnCreateTriangle.Location = new System.Drawing.Point(197, 87);
            this.btnCreateTriangle.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnCreateTriangle.Name = "btnCreateTriangle";
            this.btnCreateTriangle.Size = new System.Drawing.Size(56, 19);
            this.btnCreateTriangle.TabIndex = 7;
            this.btnCreateTriangle.Text = "Add";
            this.btnCreateTriangle.UseVisualStyleBackColor = true;
            this.btnCreateTriangle.Click += new System.EventHandler(this.btnCreateTriangle_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(197, 132);
            this.button2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(56, 19);
            this.button2.TabIndex = 8;
            this.button2.Text = "Cancel";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.btnCancelTriang);
            // 
            // FormTriangle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(272, 200);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.btnCreateTriangle);
            this.Controls.Add(this.tbBottomSide);
            this.Controls.Add(this.tbSideB);
            this.Controls.Add(this.tbSideA);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "FormTriangle";
            this.Text = "Triangle";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbSideA;
        private System.Windows.Forms.TextBox tbSideB;
        private System.Windows.Forms.TextBox tbBottomSide;
        private System.Windows.Forms.Button btnCreateTriangle;
        private System.Windows.Forms.Button button2;
    }
}