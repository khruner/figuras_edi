﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Text;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Fgiure.Interaces;
using Fgiure.Model;


namespace Fgiure
{
    public partial class FormSquare : Form
    {
        public FormSquare(Form form)
        {
            InitializeComponent();
            this.Owner = form;
        }

       
        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            FormIndex frm = new FormIndex();
            frm.Show();
        }

        private Square createSquare(double side)
        {
            Square square = new Square(side);
            return square;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            Square square = createSquare(Convert.ToDouble(tbSide.Text));
            ((FormIndex)Owner).AddFigure(square);
            this.Hide();
            Owner.Show();
            
            //square.getName();
            //square.getPerimeter();
            //square.getSurface();

        }
    }
}
