﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Fgiure.Interaces;
using Fgiure.Controller;


namespace Fgiure
{
    public partial class FormIndex : Form
    {
        FigureController controller = FigureController.GetInstance();

        List<Ifigure> FiguresList = new List<Ifigure>();
        public FormIndex()
        {
            InitializeComponent();
        }
       public void AddFigure(Ifigure figure) 
        { 
            FiguresList.Add(figure);
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            // Obtener el ensamblado actual
            Assembly currentAssembly = Assembly.GetExecutingAssembly();

            // Obtener todas las clases en el ensamblado actual que implementan la interfaz IMiInterfaz
            Type[] types = currentAssembly.GetTypes().Where(
                t => typeof(Ifigure).IsAssignableFrom(t) && !t.IsInterface && !t.IsAbstract).ToArray();

            // Agregar el nombre de cada clase al ComboBox
            foreach (Type type in types)
            {
                object value = comboBox1.Items.Add(type.Name);//aca pongo el nombre del combobox de la parte de formularios
            }


        }

        private void button1_Click(object sender, EventArgs e)
        {
           if (comboBox1.SelectedItem.ToString() == "Circle") //en vez de texto verificar con objetos
            {
                this.Hide();
                FormCircle frm = new FormCircle(this);
                frm.Show();
            }
           if(comboBox1.SelectedItem.ToString() == "Square") 
            {
                this.Hide();
                FormSquare frm = new FormSquare(this);
                frm.Show();
            }
           if(comboBox1.SelectedItem.ToString()=="Triangle")
            {
                this.Hide();
                FormTriangle frm = new FormTriangle(this);
                frm.Show();
            }
           
        }
        private void showList() 
        {
            dataGridView1.Rows.Clear();
            foreach(Ifigure figure in FiguresList) 
            {
                DataGridViewRow row = new DataGridViewRow();
                row.CreateCells(dataGridView1, figure.getName(), figure.getPerimeter(), figure.getSurface());
                dataGridView1.Rows.Add(row);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            showList();
        }
    }
}
